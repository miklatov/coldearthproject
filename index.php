<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css.css">
        <title></title>
    </head>
    <body>
        <?php
            //If code entered, upload to db and display grid
            if (isset($_POST['sequence'])){
                //Check code is valid
                if (!is_numeric($_POST['sequence']) || strlen($_POST['sequence'])<>36){ ?>
                    <p>Code must consist of 36 digits (<?php echo strlen($_POST['sequence'])?>)(<?php echo is_numeric($_POST['sequence'])?>)</p> 
          <?php } else { //if valid..
                    try {
                        $dsn='mysql:dbname=xxx;host=xxxx';
                        $user='xxx';
                        $pass='xxx';
                        $dbh = new PDO($dsn,$user,$pass);
                        $sth = $dbh->prepare('INSERT into coldearthcodes (code) VALUES( ? )');
                        $sth->execute(array($_POST['sequence']));
                        ?><p>Code submitted as <?php echo $_POST['sequence'] ?>: Thank you</p><?php 
                        
                        //extract data from db        
                        $dsn='mysql:dbname=xxx;host=xxxx';
                        $user='xxx';
                        $pass='xxx';
                        $dbh = new PDO($dsn,$user,$pass);
                        $sth = $dbh->prepare('SELECT code from coldearthcodes');
                        $sth->execute();
                        $codes = array();
                        while($row = $sth->fetch(PDO::FETCH_ASSOC)){
                            $code = $row[code];
                            
                       
                            for ($i=0;$i<36;++$i){
                                $num=substr($code,$i,1);
                                $codes[$i][$num]+=1;
                                
                            }
                        }
                        
                        //calculate the consensus
                        $cons = array();
                        for ($c=0;$c<36;++$c){
                            $top='';
                            $topCount=0;
                            for ($n=0;$n<10;++$n){
                                
                                if(!isset($codes[$c][$n])){
                                    $codes[$c][$n]=0;
                                }
                       
                                if ($codes[$c][$n] ==$topCount){
                                    $top.=','.strval($n);
                                }
                                else if ($codes[$c][$n]>$topCount){
                                    $top=strval($n);
                                    $topCount=$codes[$c][$n];
                                }
                            }
                            if ($topCount===0){
                                $cons[$c]="?";
                            } else{
                                $cons[$c]=$top;
                            }
                        }

                        //draw table
                        ?><table>
                            <tr>
                                <td id="bold">Number</td>
                                <?php for ($i=1;$i<37;++$i){
                                    ?><td id="bold"><?php echo $i ?></td><?php
                                }?>
                            </tr>
                            <!--#add rows-->
                            <?php for ($r=0;$r<10;++$r){
                                ?><tr>
                                    <td id="bold"><?php echo $r ?></td>
                                   <!--add data cells-->
                                  <?php for ($c=0;$c<36;++$c){
                                      ?><td><?php echo $codes[$c][$r] ?></td><?php
                                  } ?>
                                  </tr>
                                  
                                  
                            <?php } ?>
                            <!--add final row, the consensus-->
                            <tr>
                                <td id="bold">Consensus</td>
                            <?php for ($c=0;$c<36;++$c){
                                ?><td id="bold"><?php echo $cons[$c] ?></td><?php
                            } ?></tr>
                          </table>
                          ?>
                                    
                            <?php
                        
                    } catch (PDOException $e) {
                        ?><p> <?php echo 'Connection failed: ' . $e->getMessage(); ?> </p> <?php
                    }




            //If just visited page, i.e. not entered code yet, create input box and draw table
    
            } }else {?>
        <script type="text/javascript">
            window.onload = function(){
              var text_input = document.getElementById ('myTextInput');
              text_input.focus ();
              text_input.select ();
            };
        </script>
        
        <form action ="index.php" method="post">
           <p>Input Cold Earth code: <input type="text" id="myTextInput" name="sequence" maxlength='36'><br></p>
        </form>
        <?php ;
        $dsn='mysql:dbname=xxx;host=xxxx';
        $user='xxx';
        $pass='xxx';        
        $dbh = new PDO($dsn,$user,$pass);
        $sth = $dbh->prepare('SELECT code from coldearthcodes');
        $sth->execute();
        $codes = array();

        //fetch data for generating table
        while($row = $sth->fetch(PDO::FETCH_ASSOC)){
            $code = $row[code];
            
       
            for ($i=0;$i<36;++$i){
                $num=substr($code,$i,1);
                $codes[$i][$num]+=1;
                
            }
//            $codes[]=$row;
        }
        
        //calculate the consensus
        $cons = array();
        for ($c=0;$c<36;++$c){
            $top='';
            $topCount=0;
            for ($n=0;$n<10;++$n){
                
                if(!isset($codes[$c][$n])){
                    $codes[$c][$n]=0;
                }
       
                if ($codes[$c][$n] ==$topCount){
                    $top.=','.strval($n);
                }
                else if ($codes[$c][$n]>$topCount){
                    $top=strval($n);
                    $topCount=$codes[$c][$n];
                }
            }
            $cons[$c]=$top;
        }

        //draw table
        ?><table>
            <tr>
                <td id="bold">Number</td>
                <?php for ($i=1;$i<37;++$i){
                    ?><td id="bold"><?php echo $i ?></td><?php
                }?>
            </tr>
            <!--#add rows-->
            <?php for ($r=0;$r<10;++$r){
                ?><tr>
                    <td id="bold"><?php echo $r ?></td>
                   <!--add data cells-->
                  <?php for ($c=0;$c<36;++$c){
                      ?><td><?php echo $codes[$c][$r] ?></td><?php
                  } ?>
                  </tr>
                  
                  
            <?php } ?>
            <!--add final row, the consensus-->
            <tr>
                <td id="bold">Consensus</td>
            <?php for ($c=0;$c<36;++$c){
                ?><td id="bold"><?php echo $cons[$c] ?></td><?php
            } ?></tr>
          </table>

       <?php } ?>
    </body>
</html>
            









